#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

// struct for thread arguments
typedef struct
{
	pthread_mutex_t lock;
	volatile int* counter;
} pthreadArgs;

// simulate a person getting up and return to their seats 1000 times
void* counterThread(void* inst);

int main (int argc, char const *argv[])
{
  int x;
	double total;
  for(x = 0; x < 10; x++)
  {
		// shared volatile variable
  	volatile int counter = 0;

		// initialize 100 threads for 100 people
  	pthread_t threads[100];
  	pthreadArgs* args = (pthreadArgs*) malloc(sizeof(pthreadArgs));
  	pthread_mutex_init(&(args->lock), NULL);
  	args->counter = &counter;

		// start timing
    clock_t startTime = clock();

		// run 100 threads to increment shared variable
    int i;
  	for(i = 0; i < 100; i++)
  		pthread_create(&threads[i], NULL, counterThread, args);

    int j;
  	for(j = 0; j < 100; j++)
  		pthread_join(threads[j], NULL);

		// stop timing and get elapsed time
    clock_t endTime = clock();
		double elapsed = ((double) (endTime - startTime)) / CLOCKS_PER_SEC;

  	printf("Run %d\n", x + 1);
  	printf("Counter: %d\n", counter);
  	printf("Execution time (in seconds): %f\n", elapsed);

		// free thread arguments
  	pthread_mutex_destroy(&(args->lock));
  	free(args);

		// get total time for 10 runs
		total += elapsed;
  }

	// calculate average
	printf("Average execution time (in seconds): %f\n", total / 10);
}

// increment between threads with locks OUTSIDE the loop
void* counterThread(void* inst)
{
	pthreadArgs* args = (pthreadArgs*) inst;
	volatile int* counter = args->counter;

	int i;
	pthread_mutex_lock(&(args->lock));
	for(i = 0; i < 1000; i++)
		(*counter)++;
	pthread_mutex_unlock(&(args->lock));
}
