Anh Huynh's README for Concurrency Lab (L4):

The project is a contains 3 programs, each simulates 100 people in a stadium, with each person getting up and sitting back down to their seats 1000 times.
The number of time everyone gets up and down is calculated using 100 threads (to represent 100 people) to increment one volatile shared variable.
As a result, after the program runs, we expect the output to be 100 * 1000 = 100000

There are 3 versions of the program, which runs the counting function 10 times each and time each run. Following results are taken from running the programs on tux:
  1)Run: make no_locks
    Version WITHOUT mutex lock suffers from a race condition when the threads try to access and change the shared variable at the same time.
    In the process, the result stored in the shared variable is accessed simultaneously by different threads (not in the scheduling order that we come to expect.)
    This thus results in the following inaccurate results:
      Run 1
      Counter: 100000
      Execution time (in seconds): 0.018550
      Run 2
      Counter: 95148
      Execution time (in seconds): 0.012863
      Run 3
      Counter: 99405
      Execution time (in seconds): 0.012782
      Run 4
      Counter: 97442
      Execution time (in seconds): 0.011122
      Run 5
      Counter: 97809
      Execution time (in seconds): 0.012358
      Run 6
      Counter: 99942
      Execution time (in seconds): 0.011594
      Run 7
      Counter: 97322
      Execution time (in seconds): 0.012019
      Run 8
      Counter: 97876
      Execution time (in seconds): 0.008389
      Run 9
      Counter: 94736
      Execution time (in seconds): 0.009840
      Run 10
      Counter: 100000
      Execution time (in seconds): 0.008051
      Average execution time (in seconds): 0.011757

  2)Run: make locks_inside
    Version with mutex lock (which prevents race condition and only allow one thread to access the shared variable) INSIDE the incremental for loop provides the accurate result:
      Run 1
      Counter: 100000
      Execution time (in seconds): 0.373099
      Run 2
      Counter: 100000
      Execution time (in seconds): 0.186372
      Run 3
      Counter: 100000
      Execution time (in seconds): 0.255950
      Run 4
      Counter: 100000
      Execution time (in seconds): 0.296766
      Run 5
      Counter: 100000
      Execution time (in seconds): 0.244725
      Run 6
      Counter: 100000
      Execution time (in seconds): 0.326700
      Run 7
      Counter: 100000
      Execution time (in seconds): 0.233651
      Run 8
      Counter: 100000
      Execution time (in seconds): 0.230859
      Run 9
      Counter: 100000
      Execution time (in seconds): 0.263420
      Run 10
      Counter: 100000
      Execution time (in seconds): 0.288202
      Average execution time (in seconds): 0.269974
  3)Run: make locks_inside
    Version with mutex lock OUTSIDE the incremental for loop also provides the accurate result,
    with considerably better performance:
      Run 1
      Counter: 100000
      Execution time (in seconds): 0.013539
      Run 2
      Counter: 100000
      Execution time (in seconds): 0.012986
      Run 3
      Counter: 100000
      Execution time (in seconds): 0.012896
      Run 4
      Counter: 100000
      Execution time (in seconds): 0.010039
      Run 5
      Counter: 100000
      Execution time (in seconds): 0.012639
      Run 6
      Counter: 100000
      Execution time (in seconds): 0.011914
      Run 7
      Counter: 100000
      Execution time (in seconds): 0.008267
      Run 8
      Counter: 100000
      Execution time (in seconds): 0.010203
      Run 9
      Counter: 100000
      Execution time (in seconds): 0.009977
      Run 10
      Counter: 100000
      Execution time (in seconds): 0.010706
      Average execution time (in seconds): 0.011317

  Run: make clean to get rid of compiled files.
