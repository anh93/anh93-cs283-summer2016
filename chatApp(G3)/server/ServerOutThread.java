import java.io.*;
import java.net.*;
import java.util.*;

public class ServerOutThread implements Runnable
{
	Socket clientSocket;
	PrintWriter toClient;
	BufferedReader fromUser;
	Encryptor encryptor;

	// constructor
	public ServerOutThread(Socket clientSocket, Encryptor encryptor)
	{
		this.clientSocket = clientSocket;
		this.encryptor = encryptor;
	}

	public void run()
	{
		try
		{
			// initialize writer to write to socket connected to client
			toClient = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));//get outputstream

			String stringToClient;
			String encryptedString;
			// continue send message to client
			while(true)
			{
				System.out.print("Type in message to encrypt and send to client: ");
				// initialize reader and get user input
				BufferedReader fromUser = new BufferedReader(new InputStreamReader(System.in));
				stringToClient = fromUser.readLine();

				// if user type "quit!", exit
				if(stringToClient.equals("quit!"))
					break;

				// encrypt message to cipher values, returns List<Long>
				List<Long> ciphers = encryptor.encrypt(stringToClient);
				// translate List<Long> into String (delimited by space)
				encryptedString = encryptor.translate(ciphers);

				// send message to client
				System.out.println("\"" + stringToClient + "\" encrypted! Sent to client: " + encryptedString);
				toClient.println(encryptedString);
				toClient.flush();
			}

			// close socket
			clientSocket.close();
		}
		catch(Exception ex){System.out.println(ex.getMessage());}
	}
}
