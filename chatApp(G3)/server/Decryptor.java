import java.io.*;
import java.util.*;
import java.net.*;
import java.lang.*;

class Decryptor
{
  // private key
  long d;
  long c;

  // constructor
  public Decryptor(long d, long c)
  {
    this.d = d;
    this.c = c;
  }

  // decrypt List<Long> to String message
  public String decrypt(List<Long> ciphers)
  {
    String message = "";

    int i;
    for(i = 0; i < ciphers.size(); i++)
    {
      // get each character from cipher value via Ascii value
      char letter = (char) getAscii(ciphers.get(i));
      // put characters together to get a String
      message += letter;
    }

    return message;
  }

  // get Ascii value of each character from cipher value
  public long getAscii(long y)
  {
    // y^d mod c
    long aValue = 1;

    int i;
    for(i = 0; i < d; i++)
    {
      aValue *= y%c;
      aValue = aValue%c;
    }

    return aValue;
  }

  // translate List<Long> into String (delimited by space)
  public List<Long> translate(String message)
  {
    List<Long> ciphers = new ArrayList<Long>();

    String[] ciphersArray = message.split(" ");

    int i;
    for(i = 0; i < ciphersArray.length; i++)
      ciphers.add(Long.valueOf(Long.parseLong(ciphersArray[i])));

    return ciphers;
  }

} // end Decryptor
