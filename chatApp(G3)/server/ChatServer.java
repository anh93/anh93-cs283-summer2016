import java.io.*;
import java.net.*;
import java.lang.*;

public class ChatServer
{
	public static void main(String[] args) throws IOException
	{
		if(args.length == 5)
		{
			System.out.println("Server Running...");

			int port = Integer.parseInt(args[0]);
			ServerSocket serverSocket = new ServerSocket(port);
			// initialize socket for specific client by accepting connection from general socket
			Socket clientSocket = serverSocket.accept();
			System.out.println("Connected to client! You can start chatting. Type \"quit!\" to stop the program.");

			long d = Long.parseLong(args[3]);
			long dc = Long.parseLong(args[4]);
			// initialize encryptor with private key (d, c)
			Decryptor decryptor = new Decryptor(d, dc);
			// initialize thread object to receive message from server
			// argument: socket, decryptor
			ServerInThread inThread = new ServerInThread(clientSocket, decryptor);
			// initialize and execute thread
			Thread recieve = new Thread(inThread);
			recieve.start();

			long e = Long.parseLong(args[1]);
			long c = Long.parseLong(args[2]);
			// initialize encryptor with public key (e, c)
			Encryptor encryptor = new Encryptor(e, c);
			// initialize thread object to send message to server
			// argument: socket, encryptor
			ServerOutThread outThread = new ServerOutThread(clientSocket, encryptor);
			// initialize and execute thread
			Thread send = new Thread(outThread);
			send.start();
		}
		else
			System.out.println("Invalid arguments (5 needed). Please see README.txt");
	}
}
