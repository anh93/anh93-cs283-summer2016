import java.io.*;
import java.net.*;
import java.util.*;

public class ServerInThread implements Runnable
{
	Socket clientSocket;
	BufferedReader fromClient;
	Decryptor decryptor;

	// constructor
	public ServerInThread(Socket clientSocket, Decryptor decryptor)
	{
		this.clientSocket = clientSocket;
		this.decryptor = decryptor;
	}

	public void run()
	{
		try
		{
			// initialize reader to read from socket connected to client
			fromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			String stringFromClient;
			String decryptedString;
			// continue get message from slient
			while(true)
			{
				while((stringFromClient = fromClient.readLine())!= null)
				{
					// if user type "quit!", exit
					if(stringFromClient.equals("quit!"))
						{break;}

					// translate List<Long> into String (delimited by space)
					List<Long> ciphers = decryptor.translate(stringFromClient);
					// decrypt cipher values to message
					decryptedString = decryptor.decrypt(ciphers);

					// display message from client
					System.out.println("\nClient: " + stringFromClient + ". Decrypted to: " + decryptedString);//print the message from client
					System.out.print("Type in message to encrypt and send to client: ");
				}

				// close socket and exit
				clientSocket.close();
				System.exit(0);
			}
		}
		catch(Exception ex){System.out.println(ex.getMessage());}
	}
}
