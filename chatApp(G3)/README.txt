Anh Huynh's README for chatApp (G3):

This project is a chat application that contains the following components:
1) A server that can encrypt, decrypt and communicate messages to a client
2) A client that can encrypt, decrypt and communicate messages to a server
3) A key generator that can generate a pair of Public Key (e, c) and Private Key (d, c) for encryption and decryption, respectively
4) A key crack that can crack a pair of Public Key (e, c) to get its corresponding Private Key (d, c)

How to run:
1) To generate Public Key: MPRIME=6 NPRIME=22 make genkey
2) To get Private Key: E=251329 C=779 make keycrack
3) To start server: PORT=8080 E=393889 C=1027 D=529 DC=779 make server
4) To start client: SERVER=tux64-13.cs.drexel.edu PORT=8080 E=251329 C=779 D=241 DC=1027 make client
