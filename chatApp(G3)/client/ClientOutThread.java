import java.io.*;
import java.util.*;
import java.net.*;

public class ClientOutThread implements Runnable
{
	Socket socket;
	PrintWriter toServer;
	BufferedReader fromUser;
	Encryptor encryptor;

	// constructor
	public ClientOutThread(Socket socket, Encryptor encryptor)
	{
		this.socket = socket;
		this.encryptor = encryptor;
	}//end constructor

	public void run()
	{
		try
		{
			if(socket.isConnected())
			{
				System.out.println("Connected to server! You can start chatting. Type \"quit!\" to stop the program.");
				// initialize writer to write to socket connected to server
				toServer = new PrintWriter(socket.getOutputStream(), true);

				String stringToServer;
				String encryptedString;
				// continue send message to server
				while(true)
				{
					System.out.print("Type in message to encrypt and send to server: ");
					// initialize reader and get user input
					fromUser = new BufferedReader(new InputStreamReader(System.in));
					stringToServer = fromUser.readLine();

					// if user type "quit!", exit
					if(stringToServer.equals("quit!"))
						break;

					// encrypt message to cipher values, returns List<Long>
					List<Long> ciphers = encryptor.encrypt(stringToServer);
					// translate List<Long> into String (delimited by space)
					encryptedString = encryptor.translate(ciphers);

					// send message to server
					System.out.println("\"" + stringToServer + "\" encrypted! Sent to server: " + encryptedString);
					toServer.println(encryptedString);
					toServer.flush();
				}

				// close socket
				socket.close();
			}
		}
		catch(Exception e){System.out.println(e.getMessage());}
	}
}
