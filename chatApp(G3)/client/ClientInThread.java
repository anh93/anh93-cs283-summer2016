import java.io.*;
import java.util.*;
import java.net.*;

public class ClientInThread implements Runnable
{
	Socket socket;
	BufferedReader fromServer;
	Decryptor decryptor;

 	// constructor
	public ClientInThread(Socket socket, Decryptor decryptor)
	{
		this.socket = socket;
		this.decryptor = decryptor;
	}

	public void run()
	{
		try
		{
			// initialize reader to read from socket connected to server
			fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			String stringFromServer;
			String decryptedString;
			// continue get message from server
			while((stringFromServer = fromServer.readLine())!= null)
			{
				// if user type "quit!", exit
				if(stringFromServer.equals("quit!"))
					break;

				// translate List<Long> into String (delimited by space)
				List<Long> ciphers = decryptor.translate(stringFromServer);
				// decrypt cipher values to message
				decryptedString = decryptor.decrypt(ciphers);

				// display message from server
				System.out.println("\nServer: " + stringFromServer + ". Decrypted to: " + decryptedString);
				System.out.print("Type in message to encrypt and send to server: ");
			}

			// close socket and exit
			socket.close();
			System.exit(0);
		}
		catch(Exception e){System.out.println(e.getMessage());}
	}
}
