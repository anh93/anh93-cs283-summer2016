import java.io.*;
import java.util.*;
import java.net.*;
import java.lang.*;

class Encryptor
{
  // public key
  long e;
  long c;

  // constructor
  public Encryptor(long e, long c)
  {
    this.e = e;
    this.c = c;
  }

  // encrypt String message to a List<Long>
  public List<Long> encrypt(String message)
  {
    // convert message to array of individual characters
    char[] chars = message.toCharArray();
    int mLength = chars.length;
    List<Long> ciphers = new ArrayList<Long>();

    int i;
    for(i = 0; i < mLength; i++)
    {
      // get cipher value of each character (in Ascii)
      long c = getCipher((long)chars[i]);
      // add to List<Long>
      ciphers.add(Long.valueOf(c));
    }

    return ciphers;
  }

  // get cipher value of each character of a given character (in Ascii)
  // x^e mod c = (x mod c * x mod c * x mod c...) mod c
  // e times
  public long getCipher(long x)
  {
    long cipher = 1;

    int i;
    for(i = 0; i < e; i++)
    {
      cipher *= x%c;
      cipher = cipher%c;
    }

    return cipher;
  }

  // translate List<Long> into String (delimited by space)
  public String translate(List<Long> ciphers)
  {
    String decrypted = "";

    int i;
    for(i = 0; i < ciphers.size(); i++)
    {
      if(i == ciphers.size()-1)
        decrypted += ciphers.get(i);
      else
        decrypted += ciphers.get(i) + " ";
    }

    return decrypted;
  }

} // end Encryptor
