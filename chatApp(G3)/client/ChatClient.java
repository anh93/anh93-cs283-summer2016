import java.io.*;
import java.net.*;

public class ChatClient
{
	public static void main(String[] args)
	{
		if(args.length == 6)
		{
			try
			{
				String host = args[0];
				int port = Integer.parseInt(args[1]);
				// initialize socket from host and port number
				Socket socket = new Socket(host, port);

				long e = Long.parseLong(args[2]);
				long c = Long.parseLong(args[3]);
				// initialize encryptor with public key (e, c)
				Encryptor encryptor = new Encryptor(e, c);
				// initialize thread object to send message to server
				// argument: socket, encryptor
				ClientOutThread outThread = new ClientOutThread(socket, encryptor);
				// initialize and execute thread
				Thread send = new Thread(outThread);
				send.start();

				long d = Long.parseLong(args[4]);
				long dc = Long.parseLong(args[5]);
				// initialize encryptor with private key (d, c)
				Decryptor decryptor = new Decryptor(d, dc);
				// initialize thread object to receive message from server
				// argument: socket, decryptor
				ClientInThread inThread = new ClientInThread(socket, decryptor);
				// initialize and execute thread
				Thread receive =new Thread(inThread);
				receive.start();
			}
			catch(Exception e){System.out.println(e.getMessage());}
		}
		else
			System.out.println("Invalid arguments (6 needed). Please see README.txt");
	}
}
