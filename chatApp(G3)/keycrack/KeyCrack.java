import java.io.*;
import java.util.*;
import java.net.*;
import java.lang.*;

// see README and https://www.cs.drexel.edu/~wmm24/cs283_su16/assignments/network.html for details
public class KeyCrack
{
	public static void main(String[] args)
	{
		if(args.length == 2)
		{
			// public key (e, c) to compute d private key (d, c)
			long e = Long.parseLong(args[0]);
      long c = Long.parseLong(args[1]);
      if(e == c)
      {
        System.out.println("Error: Identical numbers. Please re-run with e and c values");
        System.exit(1);
      }

      // if e is prime (not a composite number), exit
      if(isPrime(e))
      {
        System.out.println("Error: e must be a composite number");
        System.exit(1);
      }

			// find m and d
      long m = findPrivate(e, c);
      long d = findModInverse(e, m);
      System.out.println("d = " + d);

      System.out.println("Public key (e,c) = (" + e + ", " + c + ")");
      System.out.println("Private key (d,c) corresponds to Public key (" + e + "," + c + ") = (" + d + ", " + c + ")");
		}
		else
			System.out.println("Invalid argument. Please enter valid e and c values");
	}

  // if a number is a prime, return true
  public static boolean isPrime(long num)
  {
    int i;
    for(i = 2; i <= (num/2); i++)
    {
      if (num % i == 0)
        return false;
    }
    return true;
  }

  // find m - needed for private key
  private static long findPrivate(long e, long c)
  {
    long m;

    while(true)
    {
      m = findCoPrime(e);
      // if composite and co-prime
      if(m == findCoPrime(c))
        return m;
    }
  }

  // find coprime of x
  private static long findCoPrime(long x)
  {
    long range = 1000000;
    Random r = new Random();

    while(true)
    {
      long rand = (long)(r.nextDouble()*range);
      if(rand > 0)
      {
        if(findGcd(x, rand) == 1)
          return rand;
      }

    }
  }

  // find gcd of 2 numbers
  private static long findGcd(long a, long b)
  {
    long t;
    while(b != 0)
    {
        t = a;
        a = b;
        b = t%b;
    }
    return a;
  }

  // find modular inverse of e mod m
  private static long findModInverse(long e, long m)
  {
    long i;
    for(i = 1; i < m; i++)
    {
      if((e*i) % m == 1)
        return i;
    }

    // cannot find modular inverse of e mod m
    return -1;
  }
}
