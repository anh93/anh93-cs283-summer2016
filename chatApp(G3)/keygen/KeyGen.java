import java.io.*;
import java.util.*;
import java.net.*;
import java.lang.*;

// see README and https://www.cs.drexel.edu/~wmm24/cs283_su16/assignments/network.html for details
public class KeyGen
{
	public static void main(String[] args)
	{
		if(args.length == 2)
		{
			// 2 numbers a and b to compute ath and bth prime numbers
			long a = Long.parseLong(args[0]);
      long b = Long.parseLong(args[1]);
      if(a == b)
      {
        System.out.println("Error: Identical numbers. Please re-run with different numbers");
        System.exit(1);
      }

			// find the prime numbers
      long aPrime = findPrime(a);
      long bPrime = findPrime(b);
      if(aPrime * bPrime <= 128)
      {
        System.out.println("Error: Numbers too small. Please re-run with greater numbers");
        System.exit(1);
      }

        System.out.println(a + "th prime number: " + aPrime);
        System.out.println(b + "th prime number: " + bPrime);

				// compute c, m, e, d respectively
        long c = aPrime * bPrime;
        long m = (aPrime - 1) * (bPrime - 1);
        System.out.println("c = " + c);
        System.out.println("m = " + m);

        long e = findEKey(m, c);
        System.out.println("e = " + e);

        long d = findModInverse(e, m);
        System.out.println("d = " + d);

        System.out.println("Public key (e,c) = (" + e + ", " + c + ")");
        System.out.println("Private key (d,c) = (" + d + ", " + c + ")");
		}
		else
			System.out.println("Invalid argument. Please enter 2 numbers to compute keys");
	}

  // find xth prime number
  public static long findPrime(long x)
  {
    int primeCounter = 0;
    long i = 2;

    while(true)
    {
      if(isPrime(i))
      {
        primeCounter++;
        if(primeCounter == x)
          return i;
      }
      i++;
    }
  }

  // if a number is a prime, return true
  public static boolean isPrime(long num)
  {
    int i;
    for(i = 2; i <= (num/2); i++)
    {
      if (num % i == 0)
        return false;
    }
    return true;
  }

  // find encryption key by finding coprime of both m and c
  private static long findEKey(long m, long c)
  {
    long encryptionKey;

    while(true)
    {
      encryptionKey = findCoPrime(m);
      // if composite and co-prime
      if(encryptionKey == findCoPrime(c) && !isPrime(encryptionKey))
        return encryptionKey;
    }
  }

  // find coprime of x
  private static long findCoPrime(long x)
  {
    long range = 1000000;
    Random r = new Random();

    while(true)
    {
      long rand = (long)(r.nextDouble()*range);
      if(rand > 0)
      {
        if(findGcd(x, rand) == 1)
          return rand;
      }

    }
  }

  // find gcd of 2 numbers
  private static long findGcd(long a, long b)
  {
    long t;
    while(b != 0)
    {
        t = a;
        a = b;
        b = t%b;
    }
    return a;
  }

  // find modular inverse of e mod m
  private static long findModInverse(long e, long m)
  {
    long i;
    for(i = 1; i < m; i++)
    {
      if((e*i) % m == 1)
        return i;
    }

    // cannot find modular inverse of e mod m
    return -1;
  }
}
