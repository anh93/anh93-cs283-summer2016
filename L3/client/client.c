#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>

#include "csapp.h"

char* constructRequest(char* host, char* address, char* request);

int main(int argc, char **argv)
{
    int clientfd, port;
    port = 80;
    rio_t rio;

    char* host = strdup(argv[1]);
    char* address = strdup(argv[2]);
    char request[MAXLINE];
    char response[MAXLINE];

    constructRequest(host, address, request);
    printf("%s\n", request);

    clientfd = Open_clientfd(host, port);
    free(host);
    free(address);

    // Associate a descriptor with a read requestfer and reset requestfer
    Rio_readinitb(&rio, clientfd);

    // write request to the server
    Rio_writen(clientfd, request, strlen(request));

    while (Rio_readlineb(&rio, response, MAXLINE) > 0)
    {
        puts(response);
        if(strcmp(response, "0/r/n") == 0)
            break;
    }

    Close(clientfd);
}

// construct HTTP request
char* constructRequest(char* host, char* address, char* request)
{
    strcpy(request, "GET ");
    strcat(request, address);
    strcat(request, " HTTP/1.1\r\nHost: ");
    strcat(request, host);
    strcat(request, "\r\n\r\n");
}
