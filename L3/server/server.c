#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "csapp.h"

int main(int argc, char **argv)
{
    struct sockaddr_in clientaddr;
	int connfd;
	rio_t rio;
	char request[MAXLINE];

    int port = atoi(argv[1]);
    int clientaddrlen = sizeof(clientaddr);

    int listenfd = Open_listenfd(port);

    while(1)
    {
        connfd = Accept(listenfd, (SA*) &clientaddr, &clientaddrlen);

        Rio_readinitb(&rio, connfd);

        // read each request from the client and send back appropriate response
        while((Rio_readlineb(&rio, request, MAXLINE)) != 0)
        {
            printf("Request: %s", request);

            // parse the request to get file name
            char* httpReq = strtok(request, " ");
            char* path = strtok(NULL, " ");
            char* protocol = strtok(NULL, " ");

            // if request is valid, send back the file requested
            if(strcmp(httpReq, "GET") == 0 && strcmp(protocol, "HTTP/1.1\\r\\n\\r\\n\r\n") == 0)
            {
                char* filePath = path + 1;

                // write file to client
                FILE* fd = fopen(filePath, "r");
                char line[1024];
        		while (fgets(line,sizeof(line), fd) != NULL)
        			Rio_writen(connfd, line, strlen(line));

        		fclose(fd);
            }
            else
                Rio_writen(connfd, "Invalid request\n", 16);

        }
        Close(connfd);
	}
}
