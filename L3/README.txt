Anh Huynh's README for Lab 2:

1) client.c: this program accepts a command-line argument representing the web server to connect to.
	Then, create a client socket that connects to the server user indicates, and sends an HTTP request to it over port 80 (hardcoded)

compile: gcc client.c csapp.c -o client.o -lpthread -g
run:
	 - make
	 - ./client.o www.google.com /index.html

2) server.c: this program accepts a port number as a command line argument, and starts an HTTP server.
This server should constantly accept() connections, read requests of the form: GET /path HTTP/1.1\r\n\r\n,
read the file indicated by /path, and return it over the connFD given from the call to accept().

compile: gcc server.c csapp.c -o server.o -lpthread -g
run server:
	 - make
	 - ./server.o [port number]
run telnet:
	 telnet tux64-14.cs.drexel.edu [port number]
