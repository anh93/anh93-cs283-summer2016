Anh Huynh's README for Mosiac Assignment:

The program creates a photograph mosaic from 10,000 sample images. The program splits the original image into small squares (same size as the sample images).
It then computes the average color of each small square, and compare to find the sample image with the shortest color distance to it.
Do this to all small squares, and we have an array of sample images that have very similar color pattern to the small squares from the original image.
The program then put these sample images onto a new canvas in the exact same position as its counterpart. And we have a mosiac!

Implementation of the program involve understanding the skeleton code and reading the documentation of OpenCV. Overall, the program was straight-forward enough,
and the result is cool because we can use our own image.

Test run:
./hw3ex am9.jpg 128 96 thumbs/*

"am9.jpg" is provided in the project folder. The resulting mosiac is also provided via a screenshot named "am9-result", since the original file was too large to be committed.
So was the "thumbs" folder which holds 10,000 sample images, under instructions of Professor Mongan.
