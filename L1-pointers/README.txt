Anh Huynh's README for Lab 1:

Implementation Details:
- q1: Create an array using malloc() and fill the array with number from 1 to 10. Print out the array afterwards.
- q2: Create an String (char array) array of 10 elements, with each element having a size of 15. Initialize each string to "Drexel CCI", with the null terminator '\0'
- q3: Initialize an array of 10 elements, fill the array with random numbers and sort them using pointer arithmetic.
Variable size represents the actual size of the array.
Variable sort_size represents the number of elements we want sorted (e.g to sort the first 5 elements only, set sort_size to 5)
- q4: Create a linked list structure in which data is an int. Fill the linked list with random numbers, then sort the linked list.
- q5: Create a dynamic array with add(), delete() and add() functions.
Add 100000 to the array, and time the program using clock() to compare the running time of the two functions.

Reflection:
Most of the questions were straight forward, but very time consuming for a C beginner like myself.
In question 4 to be exact, swapping pointers was difficult as I had to keep track of the node before and after the current node. After plenty of trials and errors though,
bubble sort was successfully implemented.

For question 5, It is evident that increasing the size of the array just by one is not as efficient as doubling the size of the array,
as the program took approximately 9-10 time longer to run with the prior method comparing to the later method.
(See "Adding results.xlxs" file for results).

In the end, however, I think the lab really helps me understand a lot more about the C language, in particular how pointers, linked list and arrays work in C.
