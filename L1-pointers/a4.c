#include <stdio.h>
#include <stdlib.h>
#include "Node.h"

struct Node* bubbleSort(struct Node* head, int size);
void printList(struct Node* head);
void fillList(struct Node* head, int size);
void append(struct Node* head, struct Node* append);
void swapNodes(struct Node* head, struct Node* a, struct Node* b); //keep track of nodes surrounding a and b?
void freeList(struct Node* head);

int main()
{
	int i;
	// needed for rand() to work
	srand(time(NULL));

	int size = 10;
	struct Node* head = (struct Node*) malloc(sizeof(struct Node));

	// call function to fill the linked list
	fillList(head, size);

	printf("Original List:\t");
	printList(head);

	// call function to sort the linked list
	head = bubbleSort(head, size);

	printf("Sorted List:\t");
	printList(head);

	// call function to free the linked list
	freeList(head);

	return 0;
}

// fill linked list with random numbers from 0 to 20
void fillList(struct Node* head, int size)
{
	int i;
	head->data = rand()%20 + 1;
	head->next = NULL;

	for(i = 0; i < size-1; i++)
	{
		struct Node* newNode = (struct Node*) malloc(sizeof(struct Node));
		newNode->data = rand()%20 + 1;
		newNode->next = NULL;
		append(head, newNode);
	}
}

// print linked list
void printList(struct Node* head)
{
	struct Node* current = head;

   //start from the beginning
   while(current != NULL)
	 {
      printf("%d ", current->data);
      current = current->next;
   }

	 printf("\n");
}

// sort linked list using bubble sort
struct Node* bubbleSort(struct Node* head, int size)
{
	struct Node* current = NULL;
	struct Node* prev = NULL;
	struct Node* next = NULL;
	int i, swapped;

	while(1)
	{
		current = head;
		prev = head;
		next = current->next;
		swapped = 0;

		for(i = 0; i < size; i++)
		{
			if(current->next != NULL)
			{
				if(current->data > next->data)
				{
					if(i == 0)
					{
						head = next;
						prev = head;
					}

					// swap out-of-order nodes
					current->next = next->next;
					prev->next = next;
					next->next = current;

					swapped = 1;
				}
				else
					current = current->next;

				if(i != 0)
					prev = prev->next;
				next = current->next;

				// printf("i:%d head:%d prev:%d current:%d next:%d swapped:%d\n", i, head->data, prev->data, current->data, next->data, swapped);
				// printList(head);
			}
		}

		// printf("head:%d prev:%d current:%d next:%d swapped:%d\n", head->data, prev->data, current->data, next->data, swapped);
		// printList(head);
		if(swapped == 0)
			break;
	}

	return head;
}

// Free Linked List
void freeList(struct Node* head)
{
	struct Node* current = NULL;

	while((current = head) != NULL)
	{
	    head = head->next;
	    free(current);
	}
}

void append(struct Node* head, struct Node* append)
{
  struct Node* current = head;

  while(current->next != NULL)
	{
    current = current->next;
  }

  current->next = append;
}
