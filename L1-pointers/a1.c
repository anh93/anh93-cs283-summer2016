#include <stdio.h>
#include <stdlib.h>

int main()
{
  int* num = (int*) malloc(10 * sizeof(int));

  int i;
  for(i = 0; i < 10; i++)
    {
      num[i] = i+1;
      printf("%d ", num[i]);
    }
  printf("\n");

  free(num);

  return 0;
}
