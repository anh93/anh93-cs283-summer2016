#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void bubbleSort(int* num, int size);

int main()
{
	int i;
	// needed for rand() to work
	srand(time(NULL));

	int size = 20;
	// initialize array num of random 20 numbers from 1 to 20
	// only the address of the first element of array
	int* num = (int*) malloc(size * sizeof(int));

	printf("Original List: ");
	for(i = 0; i < size; i++)
	{
		num[i] = (rand()%20) + 1;
		printf("%d ", num[i]);
	}
	printf("\n");

	// call function to sort the array
	bubbleSort(num, size);

	printf("Sorted List: ");
	for(i = 0; i < size; i++)
	{
		printf("%d ", num[i]);
	}
	printf("\n");

	 free(num);

	return 0;
}

// bubble sort function
void bubbleSort(int* num, int size)
{
	int i, swapped;
	int temp;
	while(1)
	{
		swapped = 0;

		for(i = 0; i < size-1; i++)
		{
			if(*(num+i) > *(num+i+1))
			{
				temp = *(num+i);
				*(num+i) = *(num+i+1);
				*(num+i+1) = temp;
				swapped = 1;
			}
		}

		if(swapped == 0)
			break;
	}

	return;
}
