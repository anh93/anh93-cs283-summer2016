#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  // pointer to the string array (pointer to address of first element in array of char-arrays)
  // can be used as the array itself (with bracket notation)
  char** words = (char**) malloc(10 * sizeof(char*));

  int i;
  for(i = 0; i < 10; i++)
  {
    // pointer to char-array (string)
    words[i] = (char*) malloc(15 * sizeof(char));
    strcpy(words[i], "Lemme Pass!");
    // word[i] is char* (address), so this doesn't work
    //words[i] = "Lemme Pass!";
    printf("%s \n", words[i]);
  }

  for(i = 0; i < 10; i++)
  {
    free(words[i]);
  }

  free(words);

  return 0;
}
