#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Array.h"

Array* arayConstructor(int size, int actualSize);
void fillArray(Array* array);
void addOne(Array* array, int value);
void addDouble(Array* array, int value);
void removeAtIndex(Array* array, int index);
int getValue(Array* array, int index);
void printArray(Array* array);

int main()
{
  // needed for rand() to work
	srand(time(NULL));

  // size of the array
  int size = 20;
  // number of slots with actual values
  int actualSize = 20;
  Array* array = arayConstructor(20, 20);

  fillArray(array);
  printArray(array);

  int value = (rand()%20) + 1;
  addOne(array, value);
  printArray(array);
  printf("Size: %d. Actual Size: %d. Element %d added to array. \n", array->size, array->actualSize, value);

  value = (rand()%20) + 1;
  addDouble(array, value);
  printArray(array);
  printf("Size: %d. Actual Size: %d. Element %d added to array. \n", array->size, array->actualSize, value);

  int index = (rand()%20) + 1;
  removeAtIndex(array, index);
  printArray(array);
  printf("Size: %d, Actual size: %d. Element at index %d removed from array.\n", array->size, array->actualSize, index);

  // // Begin timing
  // // Expanding array size by one when size is reached
  // double start, stop;
  // int amount = 100000;
  // int i;
  //
  // Array* a1 = arayConstructor(20, 20);
  // fillArray(a1);
  //
  // start = clock();
  // for(i = 0; i < amount; i++) {
  //   addOne(a1, i);
  // }
  // stop = clock();
  // printf("%10.2f : expanding array by one.\n", stop - start);
  //
  // // Begin timing
  // // Doubling size of array when size is reached
  // Array* a2 = arayConstructor(20, 20);
  // fillArray(a2);
  //
  // start = clock();
  // for(i = 0; i < amount; i++) {
  //   addDouble(a2, i);
  // }
  // stop = clock();
  // printf("%10.2f : doubling size of array.\n", stop - start);

  free(array->num);
  free(array);
  // free(a1->num);
  // free(a1);
  // free(a2->num);
  // free(a2);
}
/*******************************************************************************************************************************************************************************************/

Array* arayConstructor(int size, int actualSize)
{
  Array* array = (Array*) malloc(sizeof(Array));
  array->size = 20;
  array->actualSize = 20;
  array->num = (int*) malloc(array->size * sizeof(int));

  return array;
}

// fill array with random numbers between 1 and 20
void fillArray(Array* array)
{
  int i;
  for(i = 0; i < array->actualSize; i++)
	{
		array->num[i] = (rand()%20) + 1;
	}
}

// add one item to the array
void addOne(Array* array, int value)
{
  if(array->actualSize == array->size)
  {
    array->num = (int*) realloc(array->num, sizeof(int) * (array->size + 1));
  }

  array->num[array->actualSize] = value;
  array->actualSize++;
  array->size++;
}

// add one item to the array
void addDouble(Array* array, int value)
{
  if(array->actualSize == array->size)
  {
    array->num = (int*) realloc(array->num, sizeof(int) * (array->size * 2));
  }

  array->num[array->actualSize] = value;
  array->actualSize++;
  array->size *= 2;
}

// remove element at random index
// to be tested
void removeAtIndex(Array* array, int index)
{
  if(index > array->actualSize-1 || index < 0)
    printf("Invalid index.");
  else
  {
    int i;

    for(i = index - 1; i < (array->actualSize); i++) {
      array->num[i] = array->num[i+1];
    }
    array->actualSize--;
  }
}

void printArray(Array* array)
{
  int i;
  for(i = 0; i < array->actualSize; i++)
	{
		printf("%d ", array->num[i]);
	}
  printf("\n");
}
