typedef struct HashNode HashNode;

struct HashNode
{
  Node* bucket;
  int hashNum;
  HashNode* next;
};
