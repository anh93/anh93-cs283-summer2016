#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Node.h"
#include "HashNode.h"

int hash(char* word);
HashNode* buildHashTable();
void appendBucket(HashNode* head, HashNode* append);
void appendWord(Node* head, Node* append);
char* str_replace(char *orig, char *rep, char *with);
void printTable(HashNode* head);
void printBucket(Node* head);
Node* findBucket(HashNode* head, char* inputWord);
Node* findAnagrams(Node* bucket, char* inputWord);
Node* findCrosswords(Node* bucket, char* inputWord, char* fixedChar, int index);
void sort(char* word);
void freeTable(HashNode* head);
void freeBucket(Node* head);
void freeAnagrams(Node* head);

int main(void)
{
    HashNode* head = buildHashTable();

    // prompt user to choose an option
    char option[3];
    printf("Press 1 for Anagram Finder, Press 2 for Crossword Solver:\n");
    fgets(option, 3, stdin);

    // prompt user to enter a word
    char input[128];
    printf("Enter a word:\n");
    fgets(input, 128, stdin);
    // replace newline character with null terminator
    char* inputWord = str_replace(input, "\n", "\0");

    // find bucket where input word belongs
    Node* bucket = findBucket(head, inputWord);
    //printBucket(bucket);

    Node* anagrams = NULL;
    // if user chose Crosswords Solver (2)
    if(strcmp(option, "2\n") == 0)
    {
      char fixedChar[3];
      printf("Enter a character:\n");
      fgets(fixedChar, 3, stdin);
      char* fFixedChar = str_replace(fixedChar, "\n", "\0");

      int index;
      printf("Enter the index where that character should be at (index starts at 0):\n");
      scanf("%d", &index);

      anagrams = findCrosswords(bucket, inputWord, fFixedChar, index);
      free(fFixedChar);
    }
    // if user chose Anagrams Finder (any numbers other than 2)
    else
      anagrams = findAnagrams(bucket, inputWord);

    printBucket(anagrams);
    freeAnagrams(anagrams);
    //freeBucket(anagrams);

    free(inputWord);
    freeTable(head);
}

// construct a hash table
// put dictionary words in dictionary according to its hash number
HashNode* buildHashTable()
{
  HashNode* head = (HashNode*) malloc(sizeof(HashNode));
  head->bucket = NULL;
  head->hashNum = 0;
  head->next = NULL;

  HashNode* current;
  int hashNum;

  FILE* dict = fopen("/usr/share/dict/words", "r"); //open the dictionary for read-only access
  //FILE* dict = fopen("words", "r"); // for local testing
  if(dict == NULL)
      return NULL;

  char* word = (char*) malloc(128 * sizeof(char));
  while(fgets(word, 128, dict) != NULL)
  {
      current = head;
      // find hash value of each word
      hashNum = hash(word);

      // f(ormatted)Word with null terminator at the end
      char* fWord = str_replace(word, "\n", "\0");

      // create new Node to store new word
      Node* newWord = (Node*) malloc(sizeof(Node));
      if(fWord != NULL)
      {
        newWord->word = strdup(fWord);
        newWord->next = NULL;
        free(fWord);
      }

      // traverse through hash table (linked list of HashNode)
      // append word node to the corresponding bucket (linked list with the same hash number)
      while(current != NULL)
      {
        if(hashNum == current->hashNum)
        {
            appendWord(current->bucket, newWord);
            break;
        }
        // if cannot find matching hash number, and reach the end of hash table, create a new bucket
        else if(current->next == NULL)
        {
          HashNode* newBucket = (HashNode*) malloc(sizeof(HashNode));

          newBucket->bucket = newWord;
          newBucket->hashNum = hashNum;
          newBucket->next = NULL;
          // append bucket to hash table
          appendBucket(head, newBucket);
          break;
        }

        current = current->next;
      }
  }
  free(word);

  fclose(dict);
  return head;
}

// find the hash number of a word
int hash(char* word)
{
  char* temp = strdup(word);
  int bucketNum = 255;
  int i, currentLetter, sum;
  sum = 0;

  for(i = 0; i < strlen(temp); i++)
  {
    currentLetter = temp[i];

    // ignore speacial characters
    if(currentLetter < 65 || (currentLetter >= 91 && currentLetter <= 96) || currentLetter > 122)
    {
      continue;
    }
    // turn uppercase letter to lowercase
    else if(currentLetter >= 65 && currentLetter <= 90)
    {
      temp[i] += 32;
      currentLetter += 32;
    }

    sum += (currentLetter - 96);
  }
  if(sum > bucketNum - 1)
    sum = bucketNum - 1;

  free(temp);
  return sum;
}

// append word to the bucket (linked list of Node)
void appendWord(Node* head, Node* append)
{
  Node* current = head;

  while(current->next != NULL)
  {
    current = current->next;
  }

  current->next = append;
}

// append bucket to the hash table (linked list of Node)
void appendBucket(HashNode* head, HashNode* append)
{
  HashNode* current = head;

  while(current->next != NULL)
  {
    current = current->next;
  }

  current->next = append;
}

// function to replace strings
// taken from http://stackoverflow.com/questions/779875/what-is-the-function-to-replace-string-in-c
char *str_replace(char *orig, char *rep, char *with) {
  char *result, *ins, *tmp;
  int len_rep, len_with, len_front, replacements_count;

  // if original string isn't defined, abort routine
  if (!orig) {
    return NULL;
  }

  // length of string to be replaced
  len_rep = (int) strlen(rep);
  if (!rep || !len_rep) {
    return NULL;
  }

  // is string to be replaced even in original string?
  ins = strstr(orig, rep);
  if (ins == NULL) {
    return NULL;
  }

  // if no replacement string exists, replace with empty string
  if (!with) {
    with = "";
  }

  // length of replacement string
  len_with = (int) strlen(with);
  for (replacements_count = 0; (tmp = strstr(ins, rep)); ++replacements_count) {
      ins = tmp + len_rep;
  }

  tmp = result = (char*) malloc(strlen(orig) + (len_with - len_rep) * replacements_count + 1);

  if (!result) {
    return NULL;
  }

  while (replacements_count--) {
    ins = strstr(orig, rep);
    len_front = (int)(ins - orig);
    tmp = strncpy(tmp, orig, len_front) + len_front;
    tmp = strcpy(tmp, with) + len_with;
    orig += len_front + len_rep;
  }
  strcpy(tmp, orig);
  return result;
}

// print the whole Hash Table (for debugging purpose)
void printTable(HashNode* head)
{
  HashNode* current = head;
  while(current != NULL)
  {
    if(current->bucket != NULL)
    {
      printf("Hash Value: %d\n", current->hashNum);
      printBucket(current->bucket);
    }
    current = current->next;
  }
  printf("\n");
}

// print each bucket
void printBucket(Node* head)
{
  Node* current = head;
  while(current != NULL)
  {
    if(current->word != NULL)
    {
      printf("%s\n", current->word);
    }
    current = current->next;
  }
  printf("\n");
}

// find bucket where a word belongs to
Node* findBucket(HashNode* head, char* inputWord)
{
  int hashNum = hash(inputWord);

  HashNode* current = head;
  while(current != NULL)
  {
    if(hashNum == current->hashNum)
    {
        return current->bucket;
    }
    current = current->next;
  }

  return NULL;
}

// find all anagrams of a given word
// takes in bucket of words with the same hash number
Node* findAnagrams(Node* bucket, char* inputWord)
{
  Node* anagrams = (Node*) malloc(sizeof(Node));
  // the first element of this bucket stores a title(Anagram(s):) instead of the 1st anagram
  anagrams->word = "Anagram(s):";
  anagrams->next = NULL;

  char* inputCopy = strdup(inputWord);
  sort(inputCopy);

  Node* current = bucket;
  while(current != NULL)
  {
    char* wordCopy = strdup(current->word);
    sort(wordCopy);

    // compare the sorted versions of each word (2 words are anagram if they are the same after being sorted)
    if(strcmp(inputCopy, wordCopy) == 0)
    {
      Node* new = (Node*) malloc(sizeof(Node));
      new->word = current->word;
      new->next = NULL;

      appendWord(anagrams, new);
    }
    current = current->next;
    free(wordCopy);
  }

  free(inputCopy);
  return anagrams;
}

// find all anagrams of a given word, where a given character is fixed at a given index
Node* findCrosswords(Node* bucket, char* inputWord, char* fixedChar, int index)
{
  Node* anagrams = (Node*) malloc(sizeof(Node));
  anagrams->word = "Crossword(s):";
  anagrams->next = NULL;

  char* inputCopy = strdup(inputWord);
  sort(inputCopy);

  Node* current = bucket;
  while(current != NULL)
  {
    char* wordCopy = strdup(current->word);
    sort(wordCopy);

    // find words that are anagrams and whose character is at the right place
    if(strcmp(inputCopy, wordCopy) == 0 && current->word[index] == *fixedChar)
    {
      Node* new = (Node*) malloc(sizeof(Node));
      new->word = current->word;
      new->next = NULL;

      appendWord(anagrams, new);
    }
    current = current->next;
    free(wordCopy);
  }

  free(inputCopy);
  return anagrams;
}

// sort a string in alphabetical order
void sort(char* word)
{
  char temp;
  int i, j;
  size_t length = strlen(word);
  for(i = 0; i < length - 1; i++)
  {
    for (j = i + 1; j < length; j++)
    {
      if (word[i] > word[j])
      {
        temp = word[i];
        word[i] = word[j];
        word[j] = temp;
      }
    }
  }
}

// free hash tale (linked list of HashNode)
void freeTable(HashNode* head)
{
	HashNode* current = NULL;

	while((current = head) != NULL)
	{
	    head = head->next;
      freeBucket(current->bucket);
      //free(current->bucket);
	    free(current);
	}
}

// free bucket (linked list of Node)
void freeBucket(Node* head)
{
	Node* current = NULL;

	while((current = head) != NULL)
	{
	  head = head->next;
      free(current->word);
      free(current);

      // // if this is the anagrams bucket
      // if(strcmp(title, "Anagram(s):") == 0 || strcmp(title, "Crossword(s):") == 0)
      // {
      //   printf("%s\n", title);
      // }
      // else
      // {
      //   printf("yes\n");
      //   free(current->word);
      // }
	}
}

// free anagrams bucket (linked list of Node)
// doesn't free the word, since it was never malloc'd
void freeAnagrams(Node* head)
{
	Node* current = NULL;

	while((current = head) != NULL)
	{
	  head = head->next;
      free(current);
	}
}
