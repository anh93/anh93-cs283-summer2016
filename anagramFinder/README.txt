Anh Huynh's README for Anagram Solver:

Approach:
-Implement Hash Table using a Linked list of HashNode, with each node having:
  HashNode* next
  hash number
  Node* (pointer to linked list of struct Node* that stores the word)
- Load words in dictionary into Hash Table (buildHashTable())
  Open and read words from the dict file
  Put word in corresponding bucket based on hash number
- Get a word from the user
- Figure out the word's hash number, and thus which bucket it belongs to
- Search the bucket for anagrams (or crosswords, based on user's choice)
(by sorting strings and look for matches, since words are anagrams if their sorted selves are the same)
- Put these anagram in a new Bucket and print it out

The hardest part for me was to construct the Hash Table properly.
Exposure from the 1st lab definitely helps.
After words, finding anagrams and crosswords was quite straight forward.
