typedef struct Node Node;

struct Node
{
  char* word;
  Node* next;
};
