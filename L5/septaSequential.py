# -*- coding: utf-8 -*-

# python septaSequential.py otp.csv
# time python septaSequential.py otp.csv

import csv
import sys

def main():
    # initialize outer dictionary
    data = dict()

    # open the file, read in each row
    with open(sys.argv[1], 'rb') as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            stop = row["next_station"]
            # initialize inner dictionary for each stop
            if not (stop in data):
                data[stop] = dict()

            # find the train line of the specific train
            train_id = row["train_id"]
            if train_id.isdigit():
                train_line = int(train_id)%10 + 1
            else:
                train_line = 1

            # status of each train in the train line at the stop
            status = ''
            if not (train_line in data[stop]):
                # initialize train line array of each stop
                # contain status of all trains in the train line at each stop
                data[stop][train_line] = []
            if len(row['status']) > 0:
                # set status according to status of the specific train in the train line at each stop
                if row['status'] == "None":
                    status = "late"
                elif row['status'] == "On Time":
                    status = "on time"
                else:
                    status_list = row['status'].split()
                    if int(status_list[0]) > 5:
                        status = "on time"
                    else:
                        status = "late"
                # a append train status at this stop to the train line array
                data[stop][train_line].append(status)

    # find and print the on-time percentage of each train line at each stop
    for stop in data:
        for train_line in data[stop]:
            if len(data[stop][train_line]) > 0:
                num_on_time = 0
                for status in data[stop][train_line]:
                    if status == "on time":
                        num_on_time += 1
                percent_on_time = float(num_on_time) / len(data[stop][train_line]) * 100
                print "For ", stop, " train line", train_line, ", on-time percentage is ", '%0.2f' % percent_on_time,"%"

if __name__ == "__main__":
    main()
