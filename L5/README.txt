Anh Huynh's README for Map Reduce Lab (L5):

This program uses the Kaggle SEPTA on-time-performance dataset (otp.csv) to determine what percentage of trains
are within 5 minutes of on-time at each stop, separated by each train line.

How to run:
  Sequential version: make sequential
                      or python septaSequential.py otp.csv
  Parallel version: make parallel
                      or python septaParallel.py otp.csv 8
How to time:
  Sequential version: make time_sequential
                      or time python septaSequential.py otp.csv
  Parallel version: make time_parallel
                      or time python septaParallel.py otp.csv 8                    

There are 2 versions of the program. The following run time is recorded on tux:
  Sequential version (without map reduce)
    real    0m11.578s
    user    0m11.509s
    sys     0m0.060s
  MapReduce version:
    2 nodes:
      real    0m18.845s
      user    0m30.438s
      sys     0m2.019s
      Analysis: As we can see, the run-time using 2 processes is actually longer than the sequential version.
                This is perhaps because of the time it takes to set up 2 processes off-set the benefit.
    4 nodes:
      real    0m13.622s
      user    0m31.322s
      sys     0m1.947s
      Analysis: While still slower than running the program sequentially, we can see an improvement when using more processes.
                Again, the time it takes to set up 4 processes still off-set to boost in performance.
    8 nodes:
      real    0m12.261s
      user    0m32.429s
      sys     0m2.910s
      Analysis: An improvement in time. Though still not as fast as the sequential version.

    16 nodes:
      real    0m8.242s
      user    0m29.511s
      sys     0m2.758s
      Analysis: Finally, with 16 nodes, we have achieved a considerable boost of performance with multiprocessing.
