# -*- coding: utf-8 -*-

# python septaParallel.py otp.csv 16
# time python septaParallel.py otp.csv 16

import sys
from multiprocessing import Pool
import time

# initialize global variable header
header = ''

def main():
    global header
    # get filename and number of processes from args
    filename = sys.argv[1]
    processes = int(sys.argv[2])

    # initialize array to store csv lines
    csvlines = []
    csvfile = open(filename, 'r')
    # number of lines (excluding headers)
    lineno = 0

    for line in csvfile:
        if lineno > 0:
            csvlines.append(line)
        else:
            header = line
        lineno = lineno + 1

    # find number of lines each process must handle
    numlines = len(csvlines)
    lines_per_process = numlines / processes

    # array of processes elements
    # each element containing an array of the N'th cluster of numlines / processes lines of text
    process_data_array = []
    step = (numlines/processes) + 1
    for i in range(processes):
        start = i*step
        end = i*step + step
        process_data_array.append(csvlines[start:end])

    pool = Pool(processes=processes,)

    # map, shuffle and redue
    mapping = pool.map(mapper, process_data_array)
    shuffled = partition(mapping)
    reduced = pool.map(reducer, shuffled.items())

    # print the resulting dictionary
    # key is the stop and data element is another dictionary with key (train_line) and data element (on time percentage)
    for i in range(len(reduced)):
        for stop in reduced[i]:
            for train_line in reduced[i][stop]:
                print "For ", stop, " train line", train_line, ", on-time percentage is ", '%0.2f' % reduced[i][stop][train_line],"%"

# given a row, and a column heading, it tokenizes the first row of the CSV (the headers) by comma to find the column index, and then returns the row’s column tokenized by comma, at that column index
def getValByKey(row, col):
    global header # use the header row from the main script – it’s just row 1 of the CSV
    result = ''
    idx = 0

    for keycol in header.split(','):
        if keycol == col:
            result = row.split(',')[idx]
            break
        idx = idx + 1

    result = result.strip().replace('\'', '')
    return result

# the mapper function
# return an array of dicts of stop keys to trainline_status (on time or late)
def mapper(process_data):
    result = dict()
    for row in process_data:
        stop = getValByKey(row, "next_station")
        if not (stop in result):
            result[stop] = []

        # find the train line of the specific train
        train_id = getValByKey(row, "train_id")
        if train_id.isdigit():
            train_line = int(train_id)%10 + 1
        else:
            train_line = 1

        # status of each train in the train line at each stop
        status = ''
        if len(getValByKey(row, 'status')) > 0:
            # set status according to status of the specific train in the train line at each stop
            if getValByKey(row, 'status') == "None":
                status = "late"
            elif getValByKey(row, 'status') == "On Time":
                status = "on time"
            else:
                status_list = getValByKey(row, 'status').split()
                if int(status_list[0]) > 5:
                    status = "on time"
                else:
                    status = "late"

        val = str(train_line) + '_' + status
        result[stop].append(val)
    return result

# aggregate data into a single dict organized by key (stop)
def partition(mappings_list):
    result = dict()
    for mappings in mappings_list:
        for stop in mappings:
            for val in mappings[stop]:
                if not (stop in result):
                    result[stop] = []

                result[stop].append(val)

    return result

# iterate over the dict and find the on-time percentage of each trainline at each stop
def reducer(tuple):
    data = dict()
    result = dict()

    stop = tuple[0]
    rows = tuple[1]

    if not (stop in data):
        data[stop] = dict()
        for row in rows:
            elements = row.split('_')
            train_line = elements[0]
            status = elements[1]

            if not (train_line in data[stop]):
               data[stop][train_line] = []

            data[stop][train_line].append(status)

    for stop in data:
        if not (stop in result):
            result[stop] = dict()

        for train_line in data[stop]:
            if len(data[stop][train_line]) > 0:
                num_on_time = 0
                for status in data[stop][train_line]:
                    if status == "on time":
                        num_on_time += 1
                percent_on_time = float(num_on_time) / len(data[stop][train_line]) * 100
                result[stop][train_line] = percent_on_time

    return result

if __name__ == "__main__":
    main()
