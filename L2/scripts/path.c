#include <stdlib.h>
#include <stdio.h>
#include "csapp.h"
#include <string.h>

char* concat(char *s1, char *s2);
char* constructPath(char* dirPath, char* subDir);

int main()
{
    char dirPath[128];
    printf("Enter directory path:\n");
    fgets(dirPath, 128, stdin);
    printf("%s", dirPath);
    char subDir[128];
    printf("Enter sub-dir path:\n");
    fgets(subDir, 128, stdin);
    printf("%s", subDir);

    char* fullName = constructPath(dirPath, subDir);
    printf("%s\n", fullName);
    free(fullName);
    // strcat(fullName, dirPath);
    // strcat(fullName, "/");
    // strcat(fullName, subDir);
}

char* concat(char *s1, char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the zero-terminator
    //in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

char* constructPath(char* dir, char* subDir)
{
    char* subPath = concat(dir, "/");
    char* fullPath = concat(subPath, subDir);
    free(subPath);

    return fullPath;
}
