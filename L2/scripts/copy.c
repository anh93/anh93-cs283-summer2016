#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

void copy(const char *src, const char *des);

int main(int argc, char *argv[])
{
    copy(argv[1], argv[2]);
    printf("Copying src %s des %s\n", argv[1], argv[2]);
}

// copy file from sourse to destination
void copy(const char *src, const char *des)
{
    int fd_des, fd_src;
    char buf[4096];
    ssize_t nread;
    int saved_errno;

    fd_src = open(src, O_RDONLY);
    if (fd_src < 0)
        printf("Cannot open source.\n");

    fd_des = open(des, O_WRONLY | O_CREAT | O_EXCL, 0666);
    if (fd_des < 0)
        printf("Cannot open destination.\n");

    while (nread = read(fd_src, buf, sizeof buf), nread > 0)
    {
        char *out_ptr = buf;
        ssize_t nwritten;

        do {
            nwritten = write(fd_des, out_ptr, nread);

            if (nwritten >= 0)
            {
                nread -= nwritten;
                out_ptr += nwritten;
            }
        } while (nread > 0);
    }

    if (nread == 0)
    {
        if (close(fd_des) < 0)
        {
            fd_des = -1;
            printf("Cannot close destination.\n");
        }
        close(fd_src);

    }

    close(fd_src);

    if (fd_des >= 0)
        close(fd_des);
}
