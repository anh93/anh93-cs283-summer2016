typedef struct FileNode FileNode;

struct FileNode
{
  char* name;
  char* path;
  char* parent;
  char* type;
  FileNode* next;
  // pointer to sub files if a directory
  // NULL is a file
  FileNode* subFiles;
};
