Anh Huynh's README for Lab 2:

The program synchronize 2 given directorie, such that:
	If a file in a does not exist in b, you should replicate it in b.
	If a file in b does not exist in a, it should be deleted from b.
	If a file exists in both a and b, the file with the most recent modified date / time should be copied from one directory to the other.
	Note: if a directory exists in b and not in a, leave it alone (under professor Mongan's instructions)

I also provided a "test_dir" (inside test_dir.zip) to test the program. The "a" and "b" directories are both in "test_dir".
A "test_dir_result" is also provided to show what the directories should look like after running the program.
"scripts" folder only contains script used for testing can can be ignored.

Once "test_dir" is extracted, the program can be compiled using Makefile:
	DIR1=./test_dir/a DIR2=./test_dir/b make sync

Otherwise, compile using:
	gcc sync.c -o sync csapp.c -lpthread
and run with:
	./sync ./test_dir/a ./test_dir/b

One thing to note is that whenever I complie the program in Windows, it gives the error:
	In file included from csapp.h:23:0,
	                 from sync.c:3:
	csapp.h:51:12: warning: ‘h_errno’ redeclared without dllimport attribute: previous dllimport ignored [-Wattributes]
	 extern int h_errno;    /* defined by BIND for DNS errors */
	            ^
	In file included from csapp.h:23:0,
	                 from csapp.c:2:
	csapp.h:51:12: warning: ‘h_errno’ redeclared without dllimport attribute: previous dllimport ignored [-Wattributes]
	 extern int h_errno;    /* defined by BIND for DNS errors */
            ^
In Unix, however, the program compiles fine without errors.
Even though it doesn't affect the program in any ways, I just want to make it be known to the grader.
