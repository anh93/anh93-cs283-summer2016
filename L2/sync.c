#include <stdlib.h>
#include <stdio.h>
#include "csapp.h"
#include <string.h>
#include <dirent.h>
#include "FileNode.h"

FileNode* buildFilesList(char* filePath);
void appendFile(FileNode* head, FileNode* append);
void printList(FileNode* head);
void freeList(FileNode* head);
void freeFile(FileNode* file);
void freeDir(FileNode* file);
void removeNewline(char* string);
char* concat(char *s1, char *s2);
char* constructPath(char* dir, char* subDir);
FileNode* contains(FileNode* head, FileNode* file);
void syncDirs(FileNode* headA, FileNode* headB);
void copy(const char *src, const char *des);
void copyFilesInDir(FileNode* head, char* desDir);
void syncFiles(FileNode* fileA, FileNode* fileB);

int main(int argc, char *argv[])
{
    if(argc == 3)
    {
        // removeNewline(argv[1]);
        // removeNewline(argv[2]);

        // build file linked list for a
        FileNode* headA = buildFilesList(argv[1]);

        FileNode* headB = buildFilesList(argv[2]);

        // printList(headA);
        // printf("<--------------------------------------------------------------->\n");
        // printList(headB);

        // build file linked list for b
        syncDirs(headA, headB);

        freeList(headA);
        freeList(headB);
    }
    else
        printf("Invalid number of arguments (expecting 2 file paths)");
}

void removeNewline(char* string)
{
    char *pos;
    if ((pos=strchr(string, '\n')) != NULL)
        *pos = '\0';
}

// build linked list for directory
FileNode* buildFilesList(char* filePath)
{
    // initialize head pointer
    FileNode* head = (FileNode*) malloc(sizeof(FileNode));
    head->name = NULL;
    head->path = NULL;
    head->parent = NULL;
    head->type = NULL;
    head->next = NULL;
    head->subFiles = NULL;

    DIR *dir;
    struct dirent *de;
    struct stat stat;

    if ((dir = opendir(filePath)) != NULL)
    {
        /* print all the files and directories within directory */
        while ((de = readdir (dir)) != NULL)
        {
            //printf("%s\t\t\t\t%s\n", filePath, de->d_name);
            char* currentFile = constructPath(filePath, de->d_name);

            Stat(currentFile, &stat);

            // ignore "." and ".."
            if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            {}
            else
            {
                // if a file
                if(S_ISREG(stat.st_mode))
                {
                    FileNode* file = (FileNode*) malloc(sizeof(FileNode));
                    file->name = strdup(de->d_name);
                    file->path = strdup(currentFile);
                    file->parent = strdup(filePath);
                    file->type = "file";
                    file->next = NULL;
                    file->subFiles = NULL;

                    appendFile(head, file);
                }

                // if a dir
                else if(S_ISDIR(stat.st_mode))
                {
                    FileNode* dir = (FileNode*) malloc(sizeof(FileNode));
                    dir->name = strdup(de->d_name);
                    dir->path = strdup(currentFile);
                    dir->parent = strdup(filePath);
                    dir->type = "dir";
                    dir->next = NULL;

                    // recursive call to build linked list for subfiles
                    dir->subFiles = buildFilesList(currentFile);

                    appendFile(head, dir);
                }
            }
            free(currentFile);
        }
        closedir (dir);
    }
    else
    {
        /* could not open directory */
        printf ("Could not open %s.\n", filePath);
    }

    // get rid of and free placeholder
    FileNode* temp = head;
    head = head->next;
    free(temp);

    return head;
}

// concat strings
char* concat(char *s1, char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

// construct path given 2 strings (put a "/" between them)
char* constructPath(char* dir, char* subDir)
{
    char* subPath = concat(dir, "/");
    char* fullPath = concat(subPath, subDir);
    free(subPath);

    return fullPath;
}

// append file to linked list
void appendFile(FileNode* head, FileNode* append)
{
    FileNode* current = head;

    while(current->next != NULL)
    {
      current = current->next;
    }

    current->next = append;
}

// print linked list (for testing)
void printList(FileNode* head)
{
    FileNode* current = head;
    while(current != NULL)
    {
        // can change name to path to print paths
        if(current->name != NULL)
        {
            if(strcmp(current->type, "file") == 0)
                printf("%s: %s\n", current->name, current->parent);
            else
            {
                printf("%s: %s\n", current->name, current->parent);
                printf("\t\tSubfiles of %s\n", current->name);
                printList(current->subFiles);
                printf("\t\tEnd Subfiles of %s\n", current->name);
            }
        }
        current = current->next;
    }
    printf("\n");
}

// free linked list
void freeList(FileNode* head)
{
	FileNode* current = NULL;

	while((current = head) != NULL)
	{
        head = head->next;
        if(strcmp(current->type, "file") == 0)
            freeFile(current);
        else
            freeDir(current);
	}
}

// free a file
void freeFile(FileNode* file)
{
	free(file->name);
    free(file->path);
    free(file->parent);
    free(file);
}

// free a dir
void freeDir(FileNode* dir)
{
	free(dir->name);
    free(dir->path);
    free(dir->parent);
    freeList(dir->subFiles);
    free(dir);
}

// if a dir contains a file
FileNode* contains(FileNode* head, FileNode* file)
{
    FileNode* current = head;
    while(current != NULL)
    {
        if(strcmp(current->name, file->name) == 0)
            return current;
        current = current->next;
    }
    return NULL;
}

/* synchronize directories:
    If a file in a does not exist in b, you should replicate it in b.
    If a file in b does not exist in a, it should be deleted from b. (except for directories)
    If a file exists in both a and b, the file with the most recent modified date / time should be copied from one directory to the other.
*/
void syncDirs(FileNode* headA, FileNode* headB)
{
    // examine files in a (to possibly copy to b)
    FileNode* currentA = headA;
    while(currentA != NULL)
    {
        // look for the file in a that is the same as a file in b
        FileNode* fileInB = contains(headB, currentA);

        // find files in a that are not in b
        if(fileInB == NULL)
        {
            if(strcmp(currentA->type, "file") == 0)
            {
                printf("File \"%s\" not in \"%s\". Copying to \"%s\"...\n", currentA->name, headB->parent, headB->parent);
                char* fileDesPath = constructPath(headB->parent, currentA->name);
                copy(currentA->path, fileDesPath);
                free(fileDesPath);
            }
            else
            {
                printf("Dir \"%s\" not in \"%s\". Copying to \"%s\"...\n", currentA->name, headB->parent, headB->parent);
                char* dirDesPath = constructPath(headB->parent, currentA->name);
                mkdir(dirDesPath, 0700);
                copyFilesInDir(currentA->subFiles, dirDesPath);
                free(dirDesPath);
            }
        }
        currentA = currentA->next;
    }

    // examine files in b (to possibly remove or ovewrite)
    FileNode* currentB = headB;
    while(currentB != NULL)
    {
        // look for the file in a that is the same as a file in b
        FileNode* fileInA = contains(headA, currentB);

        // find files in b that are not in a
        if(fileInA == NULL)
        {
            if(strcmp(currentB->type, "file") == 0)
            {
                printf("File \"%s\" not in \"%s\". Removing...\n", currentB->name, headA->parent);
                int rem = remove(currentB->path);
                if(rem != 0)
                    printf("Unable to delete file");
            }
            else
                printf("Dir \"%s\" not in \"%s\". Do nothing.\n", currentB->name, headA->parent);
        }
        // find files in both dirs
        else
        {
            if(strcmp(currentB->type, "file") == 0)
            {
                printf("File \"%s\" in both dirs\n", currentB->name);
                syncFiles(fileInA, currentB);
            }
            else
            {
                printf("Dir \"%s\" in both. Open dir recursively to compare.\n", currentB->name);
                printf("\t\tComparing \"%s\"\n", currentB->path);
                // if b is empty
                if(currentB->subFiles != NULL)
                    syncDirs(fileInA->subFiles, currentB->subFiles);
                else
                    copyFilesInDir(fileInA->subFiles, currentB->path);
                printf("\t\tEnd Comparing \"%s\"\n", currentB->path);
            }
        }
        currentB = currentB->next;
    }
}

// copy files to an empty dir
void copyFilesInDir(FileNode* head, char* desDir)
{
    FileNode* current = head;
    while(current != NULL)
    {
        char* fileDesPath = constructPath(desDir, current->name);
        copy(current->path, fileDesPath);
        free(fileDesPath);
        current = current->next;
    }
}

// synchronize files (overwrite older version)
void syncFiles(FileNode* fileA, FileNode* fileB)
{
    struct stat statA, statB;
    Stat(fileA->path, &statA);
    printf("Last modified in a: %s", ctime(&statA.st_mtime));
    Stat(fileB->path, &statB);
    printf("Last modified in b: %s", ctime(&statB.st_mtime));

    double diff = difftime(statA.st_mtime, statB.st_mtime);
    if(diff == 0)
    {}
    else if(diff < 0)
    {
        printf("Overwriting file in\"%s\"\n", fileA->parent);

        // remove file in a
        int rem = remove(fileA->path);
        if(rem != 0)
            printf("Unable to delete file");

        // copy over file in b
        char* fileDesPath = constructPath(fileA->parent, fileB->name);
        copy(fileB->path, fileDesPath);
        free(fileDesPath);
    }
    else
    {
        printf("Overwriting file in\"%s\"\n", fileB->parent);

        //remove file in b
        int rem = remove(fileB->path);
        if(rem != 0)
            printf("Unable to delete file");

        // copy over file in a
        char* fileDesPath = constructPath(fileB->parent, fileA->name);
        copy(fileA->path, fileDesPath);
        free(fileDesPath);
    }
}

// copy file from sourse to destination
void copy(const char *src, const char *des)
{
    int fd_des, fd_src;
    char buf[4096];
    ssize_t nread;
    int saved_errno;

    fd_src = open(src, O_RDONLY);
    if (fd_src < 0)
        printf("Cannot open source.\n");

    fd_des = open(des, O_WRONLY | O_CREAT | O_EXCL, 0666);
    if (fd_des < 0)
        printf("Cannot open destination.\n");

    while (nread = read(fd_src, buf, sizeof buf), nread > 0)
    {
        char *out_ptr = buf;
        ssize_t nwritten;

        do {
            nwritten = write(fd_des, out_ptr, nread);

            if (nwritten >= 0)
            {
                nread -= nwritten;
                out_ptr += nwritten;
            }
        } while (nread > 0);
    }

    if (nread == 0)
    {
        if (close(fd_des) < 0)
        {
            fd_des = -1;
            printf("Cannot close destination.\n");
        }
        close(fd_src);

    }

    close(fd_src);

    if (fd_des >= 0)
        close(fd_des);
}
